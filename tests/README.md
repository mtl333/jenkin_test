# how to run unit-test (shortern version)
`go test ./tests/v1`
or with color
`go test ./tests/v1 | sed ''/PASS/s//$(printf "\033[32mPASS\033[0m")/'' | sed ''/FAIL/s//$(printf "\033[31mFAIL\033[0m")/''`