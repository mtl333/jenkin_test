package tests

import (
	"reflect"
	"testing"
)

func TestGetHealth(t *testing.T) {
	t.Run("GetHealth", func(t *testing.T) {
		statusCode, _ := GetHealth()
		want := 200
		if !reflect.DeepEqual(statusCode, want) {
			t.Errorf("\n Received %v \n but want %v", statusCode, want)
		}

	})
}

func TestGetAllProduct(t *testing.T) {
	t.Run("GetAllProduct", func(t *testing.T) {
		statusCode, _ := GetAllProduct()
		want := 200
		if !reflect.DeepEqual(statusCode, want) {
			t.Errorf("\n Received %v \n but want %v", statusCode, want)
		}
	})
}
