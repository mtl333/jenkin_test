package tests

import (
	"fmt"
	"jenkin_test/tests"
	"log"
	"net/http"
)

const (
	ApplicationJSON = "application/json; charset=utf-8"
	ApplicationForm = "application/x-www-form-urlencoded; charset=utf-8"
)

var baseUrl string = "http://localhost:40001"

func GetHealth() (int, []byte) {
	payload := ``
	var headers [][]string
	headers = append(headers, []string{"Content-Type", "application/json"})

	statusCode, response, err := tests.MakeRequest(http.MethodGet, fmt.Sprintf("%s/healthz", baseUrl), headers, payload)
	if err != nil {
		log.Fatalln(err)
	}
	return statusCode, response
}

func GetAllProduct() (int, []byte) {
	payload := `{
		"limit": 10,
		"page": 1,
		"date":"2023-01-01"
	}`
	var headers [][]string
	headers = append(headers, []string{"Content-Type", "application/json"})

	statusCode, response, err := tests.MakeRequest(http.MethodPost, fmt.Sprintf("%s/product/list", baseUrl), headers, payload)
	if err != nil {
		log.Fatalln(err)
	}
	return statusCode, response
}
