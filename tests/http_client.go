package tests

import (
	"bytes"
	"io"
	"log"
	"net/http"
	"strings"
)

const (
	InternalServerError = 500
	InvalidRequest      = 400
	Success             = 200
)

func MakeRequest(method string, url string, headers [][]string, payload string) (int, []byte, error) {
	request, err := http.NewRequest(method, url, bytes.NewBuffer([]byte(payload)))
	if err != nil {
		log.Fatalln(err)
	}

	for _, header := range headers {
		request.Header.Set(header[0], header[1])
	}
	lowerCaseHeader := make(http.Header)
	for key, value := range request.Header {
		lowerCaseHeader[strings.ToLower(key)] = value
	}
	request.Header = lowerCaseHeader
	client := &http.Client{}
	resp, err := client.Do(request)
	if err != nil {
		log.Fatalln(err)
	}
	response, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}
	return resp.StatusCode, response, err
}
